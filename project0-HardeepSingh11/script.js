const classNames = {
  TODO_ITEM: 'todo-container',
  TODO_CHECKBOX: 'todo-checkbox',
  TODO_TEXT: 'todo-text',
  TODO_DELETE: 'todo-delete',
}

//keep track of item's name 
const list = document.getElementById('todo-list')
const itemCountSpan = document.getElementById('item-count')
const uncheckedCountSpan = document.getElementById('unchecked-count')


let itemId = 0
let itemCount = 0
let itemUncheckCount = 0
const toDoArray = []

function newTodo() {
  
  const itemName = prompt("Please enter a new Item To Do", 'To Do Item')
  const newListItem = buildItemSection(itemName)
  //present dsfsdfdstodo list
  toDoArray.push(newListItem)
  toDoArray.forEach(element => {
    list.appendChild(element)
  })
}

function buildItemSection(toDoItem){
  //create li
  const new_li = document.createElement("li")
  new_li.id = itemId
  itemId++ //increment id everytime

  //create checkbox
  const new_checkbox = document.createElement("input")
  new_checkbox.type = 'checkbox'
  
  //when ON, add to check, when OFF add to uncheck
  new_checkbox.onclick = function(){ 
    if(new_checkbox.checked) {
      itemUncheckCount--
      updateCountHTML()
    }
    else{
      itemUncheckCount++
      updateCountHTML()
    }
  }
  //create delete button
  const new_delete_button = document.createElement("button")
  new_delete_button.type ='button'
  new_delete_button.appendChild(document.createTextNode("delete"))
  new_delete_button.onclick = function() { 
    let index = toDoArray.indexOf(new_li);
    if (index > -1) {
      toDoArray.splice(index, 1);
      list.removeChild(new_li)
      itemUncheckCount--
      itemCount--
      updateCountHTML()
      console.log('deleted')
    }
  }
  //create paragraph
  const para = document.createElement("p");
  const textNode = document.createTextNode(toDoItem);
  para.appendChild(textNode)
  
  //append all to li
  new_li.appendChild(new_checkbox)
  new_li.appendChild(new_delete_button)
  new_li.appendChild(para)

  itemUncheckCount++
  itemCount++
  updateCountHTML()

  return new_li
}

function updateCountHTML(){
  itemCountSpan.innerHTML = itemCount
  uncheckedCountSpan.innerHTML = itemUncheckCount
}